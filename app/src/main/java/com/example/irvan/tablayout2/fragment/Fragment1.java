package com.example.irvan.tablayout2.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.irvan.tablayout2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1 extends Fragment {


    public Fragment1() {
        // Required empty public constructor
    }

    public static Fragment1 newInstance(int sectionNumber){
        Fragment1 fragment1 = new Fragment1();
        Bundle args = new Bundle();
        args.putInt("satu", sectionNumber);
        fragment1.setArguments(args);

        return fragment1;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab1, container, false);
    }

}
