package com.example.irvan.tablayout2;

import android.content.res.ColorStateList;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;

import com.example.irvan.tablayout2.adapter.FragmentAdapter;
import com.example.irvan.tablayout2.fragment.Fragment1;
import com.example.irvan.tablayout2.fragment.Fragment2;
import com.example.irvan.tablayout2.fragment.Fragment3;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tab Layout");

        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);

        tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        //viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
        tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.colorMenu)));
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void setupTabIcons(){
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_call_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_forum_black_24dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_people_black_24dp);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d("Tes", "OnTabSelected " + tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.d("Tes", "onTabUnselected " + tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.d("Tes", "onTabReselected");
            }
        });
    }

    public void setupViewPager(ViewPager viewPager){
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        adapter.addFrag(new Fragment1(), "ONE");
        adapter.addFrag(new Fragment2(), "TWO");
        adapter.addFrag(new Fragment3(), "THREE");
        viewPager.setAdapter(adapter);
    }

}
